var url = require("url"),
	querystring = require("querystring");

var passport = require('passport');
var fs = require('fs');
var path = require('path'),
  express = require('express'),
  db = require('mongoskin').db('mongodb://127.0.0.1:27017/todos');


var mongoose = require('mongoose');
var configDB = require('./passport/config/database.js');
mongoose.connect(configDB.url); // connect to our database

var app = express();
var secret = 'test' + new Date().getTime().toString()

var session = require('express-session');
app.use(require("cookie-parser")(secret));
var MongoStore = require('connect-mongo')(session);
app.use(session( {store: new MongoStore({
   url: 'mongodb://127.0.0.1:27017/test',
   secret: secret
})}));
app.use(passport.initialize());
app.use(passport.session());
var flash = require('express-flash');
app.use( flash() );

var bodyParser = require("body-parser");
var methodOverride = require("method-override");

app.use(methodOverride());
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended:false
}));
require('./passport/config/passport')(passport); // pass passport for configuration
require('./passport/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport


app.use(express.static(path.join(__dirname, 'public')));
//allTodos = [];

app.get("/", function (req, res) {
      res.redirect("/index.html");
});


app.get("/getAllTodos", function (req, res) {
  db.collection('data').find({}).toArray(function(err, items) {
		var temp = [];
		for (var i = 0; i < items.length; i++) {
			if (item[i].user === req.user.local.email) {
				temp.push(items[i]);
			}
		}
    res.send(JSON.stringify(temp));
  });
});


app.get("/editTodo", function (req, res) {
	var user = req.user.local.email;
	var id = req.query.id;
  var newText = req.query.newText;
  db.collection("data").findOne({id: id, user: user}, function(err, result){
    result.text = newText;
    db.collection("data").save(result, function(e1,r1){
      db.collection('data').find({}).toArray(function(e2, items) {
        res.send(JSON.stringify(items));
      });
    });
  });
});

app.get("/deleteTodo", function (req, res) {
  var id = req.query.id;
  var newText = req.query.newText;
	var user = req.user.local.email;
  db.collection("data").remove({id: id, user: user}, function(err, result){
    db.collection('data').find({}).toArray(function(e2, items) {
      res.send(JSON.stringify(items));
    });
  });
});


app.get("/addTodo", function (req, res) {
		var user = req.user.local.email;
		var text = req.query.text;
    var newTodo = {
      id: "id" + new Date().getTime(),
			user: user,
      text: text,
      ts:  new Date().getTime(),
      done: false
    };
    var cb = function(err0, result){
      var cb1 = function(err, items) {
        res.send(JSON.stringify(items));
      }
      db.collection('data').find({}).toArray(cb1);
    }
    db.collection("data").insert(newTodo, cb);
});

app.listen(8080);




// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.send('noauth');
}
