# Assignment 6
## Due March 27<sup>th</sup> at midnight.

Write out the authentication flow for `simpleAuth` (discussed in class) as a diagram.



You can submit pdf or png. Please make sure the resolution makes the text legible.

## Visual Inspiration
https://aws.amazon.com/blogs/mobile/understanding-amazon-cognito-user-pool-oauth-2-0-grants/ 