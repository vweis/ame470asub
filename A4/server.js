var MS = require('mongoskin');
var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var methodOverride = require('method-override');
var hostname = process.env.HOSTNAME || 'localhost';
var port = 8080;

let rootUserPass = "root:RTz8w1kLDjT6"
let vinceUserPass = "vince:vintzent"

var db = MS.db('mongodb://localhost:27017/todos'/*+'?authSource=admin'*/,
  { useNewUrlParser: true });

console.log(db)


app.get("/", function (req, res) {
      res.redirect("/index.html");
});
//{user: "vince",pwd: "vintzent",roles: [{role: "userAdmin" , db:"todos"}]}


app.get("/getAllTodos", function (req, res) {
  db.collection('data').find().toArray(function(err, items){
    console.log(items)
    res.send(JSON.stringify(items));
  });
});


app.get("/editTodo", function (req, res) {
  var id = req.query.id;
  var newText = req.query.newText;

  db.collection("data").findOne({id: id}, function(err, result){
    result.text = newText;
    db.collection("data").save(result, function(e1,r1){
      db.collection('data').find({}).toArray(function(e2, items) {
        res.send(JSON.stringify(items));
      });
    });
  });
  /*db.collection('data').find().toArray(function(err, items){
    //TODO: Take the data at the index and replace its text with newText.
    items[index].text = newText;
    res.send(JSON.stringify(items))
    console.log("Edited item and sent back markup data.")
  });*/

/*todos.findOne().toArray(function(err, result) {
      if (err) throw err;
      console.log(result);
  });
  */

  /*allTodos[index].text = newText;
  res.send(JSON.stringify(allTodos));*/
});


app.get("/addTodo", function (req, res) {
    var text = req.query.text;
    var newTodo = {
      id: "id" + new Date().getTime(),
      text: text,
      ts:  new Date().getTime(),
      done: false
    }
    //allTodos.push(newTodo);
    var cb = function(err0, result){
      console.log("Error in addTodo is" + err0)
      console.log(result)
      var cb1 = function(err, items) {
        res.send(JSON.stringify(items));
      }
      db.collection('data').find({}).toArray(cb1);
    }
    db.collection("data").insert(newTodo, cb);
    /*db.collection('data').insert(newTodo, function (err, result){
      //if (err) {throw err};
      //if (result) {console.log("Added Todo.")};
      res.send(JSON.stringify(result));
      console.log("Added item and sent back markup data.")
    });

    var callBackEx = function(err, result){
      todos.find({}).toArray(function(err, items){

      })
    }*/

    //res.send(JSON.stringify(allTodos));
});


app.get("/deleteTodo", function (req, res) {
  var id = req.query.id;
  db.collection("data").findOne({id: id}, function(err, result){
    db.collection("data").remove(result, function(e1,r1){
      db.collection('data').find({}).toArray(function(e2, items) {
        res.send(JSON.stringify(items));
        console.log("Deleted item and sent back markup data.")
      });
    });
  });
  //allTodos.splice(index, index + 1)
  //res.send(JSON.stringify(allTodos));
});




app.use(methodOverride());
app.use(bodyParser());
app.use(express.static(__dirname + '/public'));
app.use(errorHandler());

console.log("Simple static server listening at http://" + hostname + ":" + port);
app.listen(port);
