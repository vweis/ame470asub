# Assignment 8
## Due May 1<sup>th</sup>

<img src='app.png' width=400>

Starting with the class code `chatRoom`, complete the functionality of
'channels' and 'memberships' menus.


## Submission
Demo in class/finals.

## Grading
1. 5 pts : 'channels' functionality
2. 5 pts : 'memberships' functionality
3. 5 pts: CSS and design
4. 5 pts: iOS/Android app (either one) using Cordova.
