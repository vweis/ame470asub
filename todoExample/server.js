var MS = require('mongoskin');
var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var methodOverride = require('method-override');
var hostname = process.env.HOSTNAME || 'localhost';
var port = 8080;

var db = MS.db('root:RTz8w1kLD6@localhost:27017/todos');
var todos = db.collection('data');

allTodos = [];

app.get("/", function (req, res) {
      res.redirect("/index.html");
});


app.get("/getAllTodos", function (req, res) {
  db.collection('data').find().toArray(function(err, items){
    res.send(JSON.stringify(items));
  });
  //res.send(JSON.stringify(allTodos));
});


app.get("/editTodo", function (req, res) {
  var index = req.query.index;
  var newText = req.query.newText;

  db.collection('data').find().toArray(function(err, items){
    //TODO: Take the data at the index and replace its text with newText.
    items[index].text = newText;
    res.send(JSON.stringify(items))
  });

/*todos.findOne().toArray(function(err, result) {
      if (err) throw err;
      console.log(result);
  });
  */

  /*allTodos[index].text = newText;
  res.send(JSON.stringify(allTodos));*/
});


app.get("/addTodo", function (req, res) {
    var text = req.query.text;
    var newTodo = {
      id: "id" + new Date().getTime(),
      text: text,
      ts:  new Date().getTime(),
      done: false
    }
    //allTodos.push(newTodo);

    db.collection('data').insert(newTodo, function (err, result){
      if (err) throw err;
      if (result) console.log("Added Todo.");
      res.send(JSON.stringify(result));
    });

    var callBackEx = function(err, result){
      todos.find({}).toArray(function(err, items){
        
      })
    }

    //res.send(JSON.stringify(allTodos));
});


app.get("/deleteTodo", function (req, res) {
  var index = req.query.index;

  //allTodos.splice(index, index + 1)
  //res.send(JSON.stringify(allTodos));
});




app.use(methodOverride());
app.use(bodyParser());
app.use(express.static(__dirname + '/public'));
app.use(errorHandler());

console.log("Simple static server listening at http://" + hostname + ":" + port);
app.listen(port);
